import java.awt.*;
import javax.swing.*;

public class MainVickUniversity {
    //Private Data
    private double feeBalance = 25000.00;
    private JLabel lblName,
            lblAdmissionNumber,
            lblFeeBalance;
    private JTextField txtName, txtAdmissionNumber, txtFeeBalance;
    private JButton btnExit;
    private DefaultComboBoxModel cbmHostelName,
            cbmRoomNumber,
            cbmBedNumber;

    public MainVickUniversity(){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,300);
        frame.setVisible(true);
        frame.setLayout(new FlowLayout());

        JMenuBar mnuBar = new JMenuBar();
        frame.setJMenuBar(mnuBar);
        frame.setTitle("Vick University | Home of Nurturing Talents.");

        JMenu mnuDashboard = new JMenu("Dashboard");
        JMenu mnuAcademics = new JMenu("Academics");
        JMenu mnuFinancial = new JMenu("Financial");
        JMenu mnuAccommodation = new JMenu("Accommodation");

        //Add JMenu To The MenuBar
        mnuBar.add(mnuDashboard);
        mnuBar.add(mnuAcademics);
        mnuBar.add(mnuFinancial);
        mnuBar.add(mnuAccommodation);

        //Financial JMenuItem
        mnuFinancial.add(new JMenuItem("Pay Fees"));
        mnuFinancial.add(new JMenuItem("Print Receipt"));

        //Accommodation JMenuItem
        mnuAccommodation.add(new JMenuItem("Book Hostel"));
        mnuAccommodation.add(new JMenuItem("Print Accomodation Receipt"));

        //Components Settings
        lblName = new JLabel("Name:");
        lblAdmissionNumber = new JLabel("Admission Number:");
        lblFeeBalance = new JLabel("Fee Balance = ");
        btnExit = new JButton("Exit");
        txtName = new JTextField(17);
        txtAdmissionNumber = new JTextField(17);
        txtFeeBalance = new JTextField(17);

        //Add Components to the Frame
        frame.add(lblName);
        frame.add(txtName);
        frame.add(lblAdmissionNumber);
        frame.add(txtAdmissionNumber);
        frame.add(lblFeeBalance);
        frame.add(txtFeeBalance);
        frame.add(btnExit);

        //Accommodation
        cbmHostelName = new DefaultComboBoxModel();
        cbmHostelName.addElement("Select Hostel...");
        cbmHostelName.addElement("Hostel A");
        cbmHostelName.addElement("Hostel B");
        cbmHostelName.addElement("Hostel C");
        cbmHostelName.addElement("Hostel D");
        cbmHostelName.addElement("Hostel ");

    }
    public void payFees(){
        int deposit = Integer.parseInt(JOptionPane.showInputDialog("Enter Fees To Pay"));
        feeBalance = feeBalance - deposit;
    }
    public void getFeeBalance(){

    }
    public static void main(String[] s){
        MainVickUniversity studentVictor = new MainVickUniversity();
        studentVictor.payFees();
    }
}
